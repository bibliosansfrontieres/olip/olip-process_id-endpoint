The container expects to find the olip config file at `/olip`.

A possible way to achieve that is to bind mount the file:

```
docker run -d \
  --name olip_pid_editor \
  --mount type=bind,source=/etc/default/olip,target=/olip
  olip_pid_editor:latest
```
