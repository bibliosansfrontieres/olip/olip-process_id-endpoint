from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel

from app.olip_config_model import OlipConfig

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


class ProjectReference(BaseModel):
    project_name: str
    process_id: str


@app.get("/process_id")
def read_project_reference():
    """
    Read the project reference.
    The project reference is the combination
    of project name and process id from the
    OLIP configuration file.
    """
    olip_config = OlipConfig()
    try:
        project_name = olip_config.project_name
        process_id = olip_config.process_id
    except FileNotFoundError:
        raise HTTPException(status_code=500, detail="olip config file does not exist")
    return {"project_name": project_name, "process_id": process_id}


@app.put("/process_id")
def update_project_reference(project_reference: ProjectReference):
    """
    Update the project reference.
    The project reference is the combination
    of project name and process id from the
    OLIP configuration file.
    """
    olip_config = OlipConfig()
    try:
        olip_config.project_name = project_reference.project_name
        olip_config.process_id = project_reference.process_id
        olip_config.save_config_file()
    except FileNotFoundError:
        raise HTTPException(status_code=500, detail="olip config file does not exist")
    return {
        "project_name": olip_config.project_name,
        "process_id": olip_config.process_id,
    }


@app.delete("/process_id")
def delete_project_reference():
    """
    Delete the project reference.
    The project reference is the combination
    of project name and process id from the
    OLIP configuration file.
    """
    olip_config = OlipConfig()
    try:
        del olip_config.project_name
        del olip_config.process_id
        olip_config.save_config_file()
    except FileNotFoundError:
        raise HTTPException(status_code=500, detail="olip config file does not exist")
    return {
        "project_name": olip_config.project_name,
        "process_id": olip_config.process_id,
    }
