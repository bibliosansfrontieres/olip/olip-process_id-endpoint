"""
This module is the model for the OLIP config file.

The code to read and write the olip config file
is taken from the deploy.py.
No improvement is done to this code.
Quality is low.
"""
OLIP_DEFAULT_CONFIG_FILE_PATH = "/olip"


class OlipConfig:
    """
    The OlipConfig class is mainly used to set,
    get and delete the following OLIP properties :
        - project_name
        - process_id

    Changes made to the configuration are persisted
    only once the save_config_file method is called.
    This is in order to allow for transactions to be
    made. Some properties need to be set together to
    be valid. Following is a list of those:

        - project_name and process_id

    There is no automatic validation of transactions.
    It is up to the user to make sure the content
    sent is accurate in the context of OLIP configuration.
    """

    def __init__(self, path=OLIP_DEFAULT_CONFIG_FILE_PATH):
        self.olip_config_file_path = path
        self._target_project_name = None
        self._target_process_id = None
        self._load_config_file()

    @property
    def project_name(self):
        "project_name property."
        self._load_config_file()
        return self._target_project_name

    @project_name.setter
    def project_name(self, name):
        """Setter for the project_name property."""
        self._target_project_name = name

    @project_name.deleter
    def project_name(self):
        """Deleter for the project_name property."""
        self._target_project_name = None

    @property
    def process_id(self):
        "process_id property."
        self._load_config_file()
        return self._target_process_id

    @process_id.setter
    def process_id(self, pid):
        """Setter for the process_id property."""
        self._target_process_id = pid

    @process_id.deleter
    def process_id(self):
        """Deleter for the process_id property."""
        self._target_process_id = None

    def _load_config_file(self):
        with open(self.olip_config_file_path) as olip_config_file:
            for line in olip_config_file:
                key, _, value = line.partition("=")
                value = value.strip()
                if key == "target_project_name":
                    self._target_project_name = value
                elif key == "target_process_id":
                    self._target_process_id = value

    def save_config_file(self):
        olip_data = {}
        with open(self.olip_config_file_path) as olip_config_file:
            for line in olip_config_file:
                key, _, value = line.partition("=")
                olip_data[key] = value.strip()
        if self._target_project_name is not None:
            olip_data["target_project_name"] = self._target_project_name
        elif olip_data.get("target_project_name"):
            del olip_data["target_project_name"]
        if self._target_process_id is not None:
            olip_data["target_process_id"] = self._target_process_id
        elif olip_data.get("target_process_id"):
            del olip_data["target_process_id"]
        file_data = ""
        for key in olip_data:
            file_data += "{}={}\n".format(key, olip_data[key])
        with open(self.olip_config_file_path, "w") as olip_config_file:
            olip_config_file.write(file_data)
