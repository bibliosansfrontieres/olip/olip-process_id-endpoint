import pytest
from app.olip_config_model import OlipConfig

olip_config_file_content = """
domain=ideascube.io
project_name=idc-bsf-olip-fresh
master_mac_address=e4:5f:01:d5:11:e1
olip_file_descriptor=http://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/
current_process_id=7358
full_project_name=idc-bsf-olip-fresh=11:fc
mac_address=e4:5f:01:d5:11:fc
target_process_id=4567a
target_project_name=idc-bsf-olip-fresh-target
"""


olip_config_file_content_without_target_project_name = """
domain=ideascube.io
project_name=idc-bsf-olip-fresh
master_mac_address=e4:5f:01:d5:11:e1
olip_file_descriptor=http://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/
current_process_id=7358
full_project_name=idc-bsf-olip-fresh=11:fc
mac_address=e4:5f:01:d5:11:fc
target_process_id=4567a
"""


olip_config_file_content_without_target_process_id = """
domain=ideascube.io
project_name=idc-bsf-olip-fresh
master_mac_address=e4:5f:01:d5:11:e1
olip_file_descriptor=http://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/
current_process_id=7358
full_project_name=idc-bsf-olip-fresh=11:fc
mac_address=e4:5f:01:d5:11:fc
target_project_name=idc-bsf-olip-fresh-target
"""


def test_get_project_name_from_existing_file(fs):
    fs.create_file("/olip", contents=olip_config_file_content)
    olip_config = OlipConfig()
    project_name = olip_config.project_name
    assert project_name == "idc-bsf-olip-fresh-target"


def test_get_project_name_from_empty_file(fs):
    fs.create_file("/olip", contents="")
    olip_config = OlipConfig()
    project_name = olip_config.project_name
    assert project_name is None


@pytest.mark.xfail(raises=FileNotFoundError)
def test_get_project_name_from_missing_file():
    olip_config = OlipConfig()
    _ = olip_config.project_name


def test_set_project_name_from_existing_file(fs):
    fs.create_file("/olip", contents=olip_config_file_content)
    olip_config = OlipConfig()
    olip_config.project_name = "idc-bsf-olip-not-fresh"
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.project_name == "idc-bsf-olip-not-fresh"


def test_set_project_name_from_empty_file(fs):
    fs.create_file("/olip", contents="")
    olip_config = OlipConfig()
    olip_config.project_name = "idc-bsf-olip-from-empty"
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.project_name == "idc-bsf-olip-from-empty"


@pytest.mark.xfail(raises=FileNotFoundError)
def test_set_project_name_from_missing_file():
    olip_config = OlipConfig()
    with pytest.raises(FileNotFoundError):
        olip_config.project_name = "idc-bsf-olip-fresh"


def test_delete_project_name_from_existing_file(fs):
    fs.create_file("/olip", contents=olip_config_file_content)
    olip_config = OlipConfig()
    del olip_config.project_name
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.project_name is None


def test_delete_missing_project_name_from_existing_file(fs):
    fs.create_file(
        "/olip", contents=olip_config_file_content_without_target_project_name
    )
    olip_config = OlipConfig()
    del olip_config.project_name
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.project_name is None


def test_delete_project_name_from_empty_file(fs):
    fs.create_file("/olip", contents="")
    olip_config = OlipConfig()
    del olip_config.project_name
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.project_name is None


@pytest.mark.xfail(raises=FileNotFoundError)
def test_delete_project_name_from_missing_file():
    olip_config = OlipConfig()
    with pytest.raises(FileNotFoundError):
        del olip_config.project_name


def test_get_process_id_from_existing_file(fs):
    fs.create_file("/olip", contents=olip_config_file_content)
    olip_config = OlipConfig()
    process_id = olip_config.process_id
    assert process_id == "4567a"


def test_get_process_id_from_empty_file(fs):
    fs.create_file("/olip", contents="")
    olip_config = OlipConfig()
    process_id = olip_config.process_id
    assert process_id is None


@pytest.mark.xfail(raises=FileNotFoundError)
def test_get_process_id_from_missing_file():
    olip_config = OlipConfig()
    with pytest.raises(FileNotFoundError):
        _ = olip_config.process_id


def test_set_process_id_from_existing_file(fs):
    fs.create_file("/olip", contents=olip_config_file_content)
    olip_config = OlipConfig()
    olip_config.process_id = "45678a"
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.process_id == "45678a"


def test_set_process_id_from_empty_file(fs):
    fs.create_file("/olip", contents="")
    olip_config = OlipConfig()
    olip_config.process_id = "45678a"
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.process_id == "45678a"


@pytest.mark.xfail(raises=FileNotFoundError)
def test_set_process_id_from_missing_file():
    olip_config = OlipConfig()
    with pytest.raises(FileNotFoundError):
        olip_config.process_id = "1234a"


def test_delete_process_id_from_existing_file(fs):
    fs.create_file("/olip", contents=olip_config_file_content)
    olip_config = OlipConfig()
    del olip_config.process_id
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.process_id is None


def test_delete_missing_process_id_from_existing_file(fs):
    fs.create_file("/olip", contents=olip_config_file_content_without_target_process_id)
    olip_config = OlipConfig()
    del olip_config.process_id
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.process_id is None


def test_delete_process_id_from_empty_file(fs):
    fs.create_file("/olip", contents="")
    olip_config = OlipConfig()
    del olip_config.process_id
    olip_config.save_config_file()
    olip_config = OlipConfig()
    assert olip_config.process_id is None


@pytest.mark.xfail(raises=FileNotFoundError)
def test_delete_process_id_from_missing_file():
    olip_config = OlipConfig()
    with pytest.raises(FileNotFoundError):
        del olip_config.process_id
