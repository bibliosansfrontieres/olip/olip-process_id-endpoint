import pytest
from app.main import app
from fastapi.testclient import TestClient

client = TestClient(app)

olip_config_file_content = """
domain=ideascube.io
project_name=idc-bsf-olip-fresh
master_mac_address=e4:5f:01:d5:11:e1
olip_file_descriptor=http://s3.eu-central-1.wasabisys.com/olip-catalog-descriptor/prod/amd64/
current_process_id=7358
full_project_name=idc-bsf-olip-fresh=11:fc
mac_address=e4:5f:01:d5:11:fc
target_process_id=4567a
target_project_name=idc-bsf-olip-fresh
"""


def test_project_refrence_get(fs):
    fs.create_file("olip", contents=olip_config_file_content)
    response = client.get("/process_id")
    assert response.status_code == 200
    assert response.json() == {
        "project_name": "idc-bsf-olip-fresh",
        "process_id": "4567a",
    }


@pytest.mark.xfail(raises=FileNotFoundError)
def test_project_reference_get_missing_olip_config_file():
    response = client.get("/process_id")
    assert response.status_code == 500
    assert response.json() == {"detail": "olip file does not exist"}


def test_project_reference_put(fs):
    fs.create_file("olip", contents=olip_config_file_content)
    response = client.get("/process_id")
    assert response.status_code == 200
    assert response.json() == {
        "project_name": "idc-bsf-olip-fresh",
        "process_id": "4567a",
    }

    response = client.put(
        "/process_id",
        json={"project_name": "idc-bsf-olip-fresh-2", "process_id": "1234a"},
    )
    assert response.status_code == 200
    assert response.json() == {
        "project_name": "idc-bsf-olip-fresh-2",
        "process_id": "1234a",
    }

    response = client.get("/process_id")
    assert response.status_code == 200
    assert response.json() == {
        "project_name": "idc-bsf-olip-fresh-2",
        "process_id": "1234a",
    }


def test_project_reference_delete(fs):
    fs.create_file("olip", contents=olip_config_file_content)
    response = client.delete("/process_id")
    assert response.status_code == 200
    assert response.json() == {"project_name": None, "process_id": None}

    response = client.get("/process_id")
    assert response.status_code == 200
    assert response.json() == {"project_name": None, "process_id": None}
