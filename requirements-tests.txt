fastapi>=0.95.0,<0.96.0
pydantic>=1.10.0,<2.0.0
uvicorn[standard]>=0.21.0,<0.22.0
httpx>=0.23.0,<0.24.0
pytest
pyfakefs
